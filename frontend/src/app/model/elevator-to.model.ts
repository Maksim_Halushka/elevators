import {Direction} from "./direction.model";

export class ElevatorTo{
  elevatorId: number;
  direction: Direction;
  addressedFloor: number;
  currentFloor: number;
  actualRequestPool: number[];
}
