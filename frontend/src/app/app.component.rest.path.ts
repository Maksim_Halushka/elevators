export class AppComponentRestPath {

  static REST_PING: string = '/rest/v1/ping';

  static REST_CHECK: string = '/rest/v1/check';

  static REST_INIT: string = '/rest/v1/init';
}
