import {Component, OnInit} from '@angular/core';
import {AppComponentService} from "./app.component.service";
import * as SockJS from 'sockjs-client';
import {Stomp} from "@stomp/stompjs";
import {BehaviorSubject, Observable} from "rxjs";
import {ElevatorTo} from "./model/elevator-to.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  _requestPool: BehaviorSubject<number[]> = new BehaviorSubject([]);

  elevators: ElevatorTo[] = [] as ElevatorTo[];

  _elevators: BehaviorSubject<ElevatorTo[]> = new BehaviorSubject(this.elevators);

  elevators$: Observable<ElevatorTo[]>;

  private stompClient;

  constructor(private appComponentService: AppComponentService) {
    this.initializeWebSocketConnection();
    this.elevators$ = this._elevators.asObservable();
  }

  ngOnInit() {
    this.initSubscriptions();
  }


  private initializeWebSocketConnection() {
    let ws = new SockJS(this.appComponentService.baseUrl + '/socket');
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/refresh", (message) => {
        const elevator: ElevatorTo = JSON.parse(message.body);
        if (elevator) {
          that.recalculateElevators(elevator);
        }
      });
    });
  }

  private initSubscriptions() {
    this.elevators$.subscribe(
      elevators => {
        console.log(elevators);
      }
    );
  }

  private recalculateElevators(elevator: ElevatorTo) {
    const elevatorToChange = this.elevators.findIndex(value => value.elevatorId == elevator.elevatorId);
    if(elevatorToChange !== -1) {
      this.elevators[elevatorToChange] = elevator;
    } else {
      this.elevators.push(elevator);
    }
    this._requestPool.next(elevator.actualRequestPool);
    this._elevators.next(this.elevators);
  }

  init() {
    this.appComponentService.init().subscribe(value => {
      console.log(value);
    });
  }
}
