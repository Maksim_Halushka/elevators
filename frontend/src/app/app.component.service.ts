import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {AppComponentRestPath} from "./app.component.rest.path";
import {Observable} from "rxjs";

@Injectable()
export class AppComponentService {

  baseUrl: String = 'http://localhost:8080';
  private title = 'WebSockets Request Pool';
  private stompClient;

  constructor(private http: HttpClient) {}

  ping(): Observable<String> {
    return this.http.get<String>(this.baseUrl + AppComponentRestPath.REST_PING);
  }

  check(): Observable<string[]> {
    return this.http.get<string[]>(this.baseUrl + AppComponentRestPath.REST_CHECK);
  }

  init(): Observable<string> {
    return this.http.get<string>(this.baseUrl + AppComponentRestPath.REST_INIT);
  }

}
