package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.ElevatorRequestPool;
import com.tingco.codechallenge.elevator.api.ElevatorRequestPoolController;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.TreeSet;

public class ElevatorRequestPoolControllerImpl implements ElevatorRequestPoolController {

    @Autowired
    ElevatorRequestPool elevatorRequestPool;

    @Override
    public void requestElevator(Long toFloor) {
        elevatorRequestPool.addRequestedFloor(toFloor);
    }

    @Override
    public TreeSet<Long> getElevatorRequestPool() {
        return elevatorRequestPool.getRequestedFloors();
    }

}
