package com.tingco.codechallenge.elevator.constant;

public interface ApplicationConstants {

    String NUMBER_OF_FLOORS = "${com.tingco.elevator.numberOfFloors}";

    String NUMBER_OF_ELEVATORS = "${com.tingco.elevator.numberOfElevators}";

    String TIME_TO_PASS_ALL_FLOORS = "${com.tingco.elevator.timeToPassAllFloors}";

}
