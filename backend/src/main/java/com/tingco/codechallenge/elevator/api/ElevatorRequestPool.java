package com.tingco.codechallenge.elevator.api;

import org.springframework.stereotype.Component;

import java.util.TreeSet;

@Component
//@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public interface ElevatorRequestPool {

    void addRequestedFloor(Long floor);

    TreeSet<Long> getRequestedFloors();

    /**
     * Checks pool for current elevator and get closer floors to go
     * @return tree set of floors that elevator will take
     */
    TreeSet<Long> checkPool(Elevator elevator);
}
