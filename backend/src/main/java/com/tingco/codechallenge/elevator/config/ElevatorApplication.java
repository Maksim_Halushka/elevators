package com.tingco.codechallenge.elevator.config;

import com.google.common.collect.Lists;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorController;
import com.tingco.codechallenge.elevator.api.ElevatorRequestPool;
import com.tingco.codechallenge.elevator.api.ElevatorRequestPoolController;
import com.tingco.codechallenge.elevator.constant.ApplicationConstants;
import com.tingco.codechallenge.elevator.impl.ElevatorControllerImpl;
import com.tingco.codechallenge.elevator.impl.ElevatorRequestPoolControllerImpl;
import com.tingco.codechallenge.elevator.impl.ElevatorRequestPoolTo;
import com.tingco.codechallenge.elevator.impl.ElevatorTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Preconfigured Spring Application boot class.
 *
 */
@Configuration
@ComponentScan(basePackages = { "com.tingco.codechallenge.elevator" })
@EnableAutoConfiguration
@PropertySources({ @PropertySource("classpath:application.properties") })
@EnableWebSocketMessageBroker
public class ElevatorApplication extends AbstractWebSocketMessageBrokerConfigurer {

    @Value(ApplicationConstants.NUMBER_OF_ELEVATORS)
    private int numberOfElevators;

    @Value(ApplicationConstants.NUMBER_OF_FLOORS)
    private int numberOfFloors;

    @Value(ApplicationConstants.TIME_TO_PASS_ALL_FLOORS)
    private int timeToPassAllFloors;

    @Autowired
    ApplicationContext applicationContext;

    /**
     * Start method that will be invoked when starting the Spring context.
     *
     * @param args
     *            Not in use
     */
    public static void main(final String[] args) {
        SpringApplication.run(ElevatorApplication.class, args);
    }

    /**
     * Create a default thread pool for your convenience.
     *
     * @return Executor thread pool
     */
    @Bean(destroyMethod = "shutdown")
    public Executor taskExecutor() {
        return Executors.newScheduledThreadPool(numberOfElevators);
    }

    /**
     * Create an event bus for your convenience.
     *
     * @return EventBus for async task execution
     */
    @Bean
    public EventBus eventBus() {
        return new AsyncEventBus(Executors.newCachedThreadPool());
    }

    @Bean
    public ElevatorRequestPool getElevatorRequest() {
        return ElevatorRequestPoolTo.getInstance();
    }

    @Bean
    public ElevatorController getElevatorController() {
        return new ElevatorControllerImpl();
    }

    @Bean
    public ElevatorRequestPoolController getElevatorRequestPoolController() {
        return new ElevatorRequestPoolControllerImpl();
    }

    @Bean
    public List<Elevator> getElevators() {
        ArrayList<Elevator> elevatorTos = Lists.newArrayList();
        ElevatorTo.initializeStatics(timeToPassAllFloors/numberOfFloors, timeToPassAllFloors / (numberOfFloors * 3));
        for (int i = 0; i < numberOfElevators; i++) {
            ElevatorTo elevator = new ElevatorTo(ElevatorRequestPoolTo.getInstance(), applicationContext.getBean(SimpMessagingTemplate.class));
            elevatorTos.add(elevator);
        }
        return elevatorTos;
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        stompEndpointRegistry.addEndpoint("/socket")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/rest/v1")
                .enableSimpleBroker("/refresh");
    }

}
