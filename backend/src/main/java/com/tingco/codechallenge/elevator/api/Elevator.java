package com.tingco.codechallenge.elevator.api;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Interface for an elevator object.
 *
 * @author Sven Wesley
 */
@Component
@Scope(value = "prototype")
public interface Elevator extends Runnable {

    /**
     * Enumeration for describing elevator's direction.
     */
    enum Direction {
        UP, DOWN, NONE
    }

    /**
     * Tells which direction is the elevator going in.
     *
     * @return Direction Enumeration value describing the direction.
     */
    Direction getDirection();

    /**
     * If the elevator is moving. This is the target floor.
     *
     * @return primitive integer number of floor
     */
    Long getAddressedFloor();

    /**
     * Get the Id of this elevator.
     *
     * @return primitive integer representing the elevator.
     */
    Long getElevatorId();

    /**
     * Command to move the elevator to the given floor.
     *
     * @param toFloor int where to go.
     */
    void moveElevator(Long toFloor);

    /**
     * Check if the elevator is occupied at the moment.
     *
     * @return true if busy.
     */
    boolean isBusy();

    /**
     * Reports which floor the elevator is at right now.
     *
     * @return int actual floor at the moment.
     */
    Long getCurrentFloor();

    /**
     * Change current floor depending on direction
     */
    void changeCurrentFloor();

}
