package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorRequestPool;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.TreeSet;

/**
 * Implementation of elevator
 *
 * @author Maksim Halushka
 */
public abstract class ElevatorAbstractTo implements Elevator {

    public static Long LAST_GENERATED_ELEVATOR_ID = 0L;

    public static int FLOOR_CHANGE_TIME = 300;

    public static int FLOOR_WAITING_TIME = 100;

    private final Long id;

    private ElevatorRequestPool elevatorRequestPool;

    private SimpMessagingTemplate simpMessagingTemplate;

    public ElevatorAbstractTo(ElevatorRequestPool elevatorRequestPool, SimpMessagingTemplate simpMessagingTemplate) {
        this.id = ++LAST_GENERATED_ELEVATOR_ID;
        this.elevatorRequestPool = elevatorRequestPool;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @Override
    public void run() {
        while(true) {
            elevatorRequestPool.checkPool(this);
            try {
                Thread.sleep(FLOOR_CHANGE_TIME);
                System.out.println("Thread " + getElevatorId() + " goes from [" + getCurrentFloor() + "] floor to [" + getAddressedFloor() + "] floor. " +
                        "Elevator request pool is: " + elevatorRequestPool.getRequestedFloors().toString());
                changeCurrentFloor();
//                Thread.sleep(FLOOR_WAITING_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                simpMessagingTemplate.convertAndSend("/refresh", this);
                System.out.println("Thread " + getElevatorId() + " send pool to client");
            }
        }
    }

    @Override
    public Direction getDirection() {
        switch (Long.compare(getCurrentFloor(), getAddressedFloor())) {
            case -1:
                return Direction.UP;
            case 1:
                return Direction.DOWN;
            default:
                return Direction.NONE;
        }
    }

    @Override
    public Long getElevatorId() {
        return this.id;
    }

    @Override
    public boolean isBusy() {
        return !Direction.NONE.equals(getDirection());
    }

    public TreeSet<Long> getElevatorRequestPool() {
        return elevatorRequestPool.getRequestedFloors();
    }

    public static void initializeStatics(int floorChangeTime, int floorWaitingTime) {
        FLOOR_CHANGE_TIME = floorChangeTime;
        FLOOR_WAITING_TIME = floorWaitingTime;
    }
}
