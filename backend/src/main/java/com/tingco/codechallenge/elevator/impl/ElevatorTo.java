package com.tingco.codechallenge.elevator.impl;

import com.tingco.codechallenge.elevator.api.ElevatorRequestPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.TreeSet;

/**
 * Implementation of elevator
 *
 * @author Maksim Halushka
 *
 */
public class ElevatorTo extends ElevatorAbstractTo {

    private Long addressedFloor = 0L;

    private Long currentFloor = 0L;

    private TreeSet<Long> actualRequestPool;

    @Autowired
    public ElevatorTo(ElevatorRequestPool elevatorRequestPool, SimpMessagingTemplate simpMessagingTemplate) {
        super(elevatorRequestPool, simpMessagingTemplate);
    }

    @Override
    public Long getAddressedFloor() {
        return this.addressedFloor;
    }

    @Override
    public void moveElevator(Long toFloor) {
        setAddressedFloor(toFloor);
    }

    @Override
    public Long getCurrentFloor() {
        return this.currentFloor;
    }

    public void setAddressedFloor(Long addressedFloor) {
        this.addressedFloor = addressedFloor;
    }

    public TreeSet<Long> getActualRequestPool() {
        actualRequestPool = getElevatorRequestPool();
        return actualRequestPool;
    }

    @Override
    public void changeCurrentFloor() {
        switch (getDirection()) {
            case UP:
                currentFloor++;
                break;
            case DOWN:
                currentFloor--;
                break;
        }
    }

}
