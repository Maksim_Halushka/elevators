package com.tingco.codechallenge.elevator.impl;

import com.google.common.collect.Sets;
import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorRequestPool;

import java.util.TreeSet;

/**
 * @author Maksim Halushka
 */

public class ElevatorRequestPoolTo implements ElevatorRequestPool {

    private static ElevatorRequestPool elevatorRequestPool = null;

    private TreeSet<Long> floors;

    private ElevatorRequestPoolTo() {
        this.floors = Sets.newTreeSet();
    }

    static public ElevatorRequestPool getInstance() {
        if (elevatorRequestPool == null) {
            elevatorRequestPool = new ElevatorRequestPoolTo();
        }
        return elevatorRequestPool;
    }

    public void addRequestedFloor(Long floor) {
        floors.add(floor);
    }

    public TreeSet<Long> getRequestedFloors() {
        return floors;
    }

    // TODO: Here we need to change the algorithm
    @Override
    public synchronized TreeSet<Long> checkPool(Elevator elevator) {
        Long addressedFloor = Long.valueOf(elevator.getAddressedFloor());
        Long higher = this.floors.higher(addressedFloor);
        Long lower = this.floors.lower(addressedFloor);
        TreeSet<Long> comparables = Sets.newTreeSet();
        switch (elevator.getDirection()) {
            case UP: {
                if (higher != null) comparables.add(higher);
                break;
            }
            case DOWN: {
                if (lower != null) comparables.add(lower);
                break;
            }
            case NONE: {
                if (higher != null && lower != null) {
                    int compare = Long.compare(higher - addressedFloor, addressedFloor - lower);
                    if (compare == -1) {
                        comparables.add(higher);
                    } else if (compare == 1) {
                        comparables.add(lower);
                    } else {
                        comparables.add(lower);
                    }
                } else if (higher != null) {
                    comparables.add(higher);
                } else if (lower != null) {
                    comparables.add(lower);
                }
                break;
            }
        }
        if(!comparables.isEmpty()) {
            this.floors.removeAll(comparables);
            elevator.moveElevator(comparables.first());
        } else {
            try {
                if(elevator.getCurrentFloor() - elevator.getAddressedFloor() == 0)
                    elevator.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return comparables;
    }
}
