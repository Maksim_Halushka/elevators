package com.tingco.codechallenge.elevator.api;

import org.springframework.stereotype.Component;

import java.util.TreeSet;

@Component
public interface ElevatorRequestPoolController {

    void requestElevator(Long toFloor);

    TreeSet<Long> getElevatorRequestPool();

}
