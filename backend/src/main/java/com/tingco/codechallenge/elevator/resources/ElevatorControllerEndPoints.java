package com.tingco.codechallenge.elevator.resources;

import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorRequestPoolController;
import com.tingco.codechallenge.elevator.constant.ApplicationConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;
import java.util.TreeSet;
import java.util.stream.Stream;

/**
 * Rest Resource.
 *
 * @author Sven Wesley
 */
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/rest/v1")
public final class ElevatorControllerEndPoints {

    @Autowired
    List<Elevator> elevators;

    @Autowired
    ElevatorRequestPoolController elevatorRequestPoolController;

    @Value(ApplicationConstants.NUMBER_OF_FLOORS)
    private int numberOfFloors;

    @Value(ApplicationConstants.NUMBER_OF_ELEVATORS)
    private int numberOfElevators;

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @GetMapping(value = "/ping", produces="text/plain")
    @ResponseBody
    public String ping() {
        return "pong";
    }

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @GetMapping(value = "/init")
    public String init() {
        Random random = new Random();
        Stream.iterate(0, i -> i + 1).limit(numberOfFloors).forEach(
            i -> elevatorRequestPoolController.requestElevator(new Long(random.nextInt(numberOfFloors)))
        );
        runElevators();
        return elevatorRequestPoolController.getElevatorRequestPool().toString();
    }

    /**
     * Ping service to test if we are alive.
     *
     * @return String pong
     */
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public TreeSet<Long> check() {
        return elevatorRequestPoolController.getElevatorRequestPool();
    }

    private void runElevators() {
        elevators.forEach(elevator -> {
            Thread thread = new Thread(elevator);
            thread.start();
        });
    }

}
